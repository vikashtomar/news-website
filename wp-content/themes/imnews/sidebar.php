<?php
/**
 * The sidebar containing the main widget area
 *
 * 
 * @package imnews
 */

if ( ! is_active_sidebar( 'main-sidebar' ) ) {
	return;
} ?>
<div class="col-lg-3 col-md-3 col-sm-3">
    <div class="side-area">
    	<?php dynamic_sidebar( 'main-sidebar' ); ?>
    </div>
</div>
<div class="col-lg-2 adsright">
    <img src="https://s3.envato.com/files/82551447/Sales%20Website%20Banner%20Ads.jpg">
</div>